package de.openlicht.opliapp;

import android.widget.SeekBar;
import android.widget.TextView;

import com.sdsmdg.harjot.vectormaster.VectorMasterView;

/**
 * Created by gentt on 19.03.2018.
 */

public class customModel {

    private TextView description;
    private VectorMasterView animButton;
    private VectorMasterView powerButton;
    private SeekBar seekBar;

    public customModel(TextView description, VectorMasterView animButton, VectorMasterView powerButton, SeekBar seekBar) {
        this.description = description;
        this.animButton = animButton;
        this.powerButton = powerButton;
        this.seekBar = seekBar;
    }

    public TextView getDescription() {
        return description;
    }

    public void setDescription(TextView description) {
        this.description = description;
    }

    public VectorMasterView getAnimButton() {
        return animButton;
    }

    public void setAnimButton(VectorMasterView animButton) {
        this.animButton = animButton;
    }

    public VectorMasterView getPowerButton() {
        return powerButton;
    }

    public void setPowerButton(VectorMasterView powerButton) {
        this.powerButton = powerButton;
    }

    public SeekBar getSeekBar() {
        return seekBar;
    }

    public void setSeekBar(SeekBar seekBar) {
        this.seekBar = seekBar;
    }



}
