package de.openlicht.opliapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorSpace;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.sdsmdg.harjot.vectormaster.VectorMasterView;
import net.margaritov.preference.colorpicker.ColorPickerDialog;

import java.util.ArrayList;

/**
 * Created by gentt on 06.03.2018.
 */

public class ItemListAdapter extends ArrayAdapter<String> {
    private LayoutInflater layoutInflater;
    private ArrayList<String> items;
    private int layoutId;

    //private boolean powerStatus = false;
    //private boolean animStatus = false;

    // Used colors
    int oneColorToRuleThemAll = 0xFFFFFFFF; // ALPHA is required in order to work 0xFFEFFF
    int colorTransparent = 0x00000000;
    int colorBlack = 0xFF000000;
    int colorDefault = 0xFFFFFFFF;

    public ItemListAdapter(Context context, int layoutId, ArrayList<String> items){
        super(context, layoutId, items);
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layoutId = layoutId;
        this.items = items;
    }

    // Method to set progress bar color of an progress bar
    public void setProgressBarColor(ProgressBar progressBar, int newColor) {
        // grab the progressDrawable of progressBar and implement it as LayerDrawable
        LayerDrawable progressBarDrawable = (LayerDrawable) progressBar.getProgressDrawable();
        // split and determine which is the backgroundDrawable and which is the progressDrawable
        Drawable backgroundDrawable = progressBarDrawable.getDrawable(0);
        Drawable progressDrawable = progressBarDrawable.getDrawable(1);
        // set new color values to the progressBar
        backgroundDrawable.setColorFilter(ContextCompat.getColor(this.getContext(), R.color.colorSeekbarDivider), PorterDuff.Mode.SRC_IN);
        // apply the new color
        progressDrawable.setTint(newColor);
    }

    // Method to set the background color of an view
    public void setViewBackgroundColor(View view, int fadeFromColor, int fadeToColor) {
        // color array for the gradiant background
        int[] colors = new int[3];
        colors[0] = fadeFromColor;
        colors[1] = fadeToColor;
        colors[2] = fadeToColor;
        // make new gradient background with colors from the color array
        GradientDrawable newGradientBackground = new GradientDrawable(GradientDrawable.Orientation.BL_TR, colors);
        newGradientBackground.setAlpha(0xFF);
        newGradientBackground.setGradientType(GradientDrawable.LINEAR_GRADIENT);
        // apply the new background to the view
        view.setBackground(newGradientBackground);
    }

    // DEMO UPDATE METHOD FOR THE VIEWHOLDER
    public void updateViewHolder(ViewHolder vh, View view) {
        vh.textView = (TextView) view.findViewById(R.id.Description);
        vh.seekBar = (SeekBar) view.findViewById(R.id.SeekBar);
        vh.powerButton = (VectorMasterView) view.findViewById(R.id.PowerButton);
        vh.animButton = (VectorMasterView) view.findViewById(R.id.AnimButton);
    }

    public View getView(int position, View convertView, ViewGroup parent){

        // implement the convertView
        convertView = layoutInflater.inflate(layoutId, null);
        // through convertView is not accessable inside of onClick or onScroll listeners: make an final accessable one
        final View accessableView = convertView;

        // set colors for the first startup here
        setViewBackgroundColor(accessableView, colorDefault, oneColorToRuleThemAll );

        // TODO: Save current state to avoid "löschanfall" during scrolling
        //TODO: implement ViewHolder
        final ViewHolder vh = new ViewHolder();

        // implementation of the AnimButtonView to change its properties
        final VectorMasterView AnimButtonView = convertView.findViewById(R.id.AnimButton);
        AnimButtonView.getPathModelByName("AnimButtonOverlay").setFillColor(colorBlack);
        AnimButtonView.update();

        AnimButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: put animButton code here
                // TODO: save each status of each button indivitually.

                // if button is off -> turn it on
                if(AnimButtonView.getPathModelByName("AnimButtonOverlay").getFillColor()==colorBlack) {
                    AnimButtonView.getPathModelByName("AnimButtonOverlay").setFillColor(colorTransparent);
                    AnimButtonView.update();
                    // if button is on -> turn it off
                } else if(AnimButtonView.getPathModelByName("AnimButtonOverlay").getFillColor()==colorTransparent) {
                    AnimButtonView.getPathModelByName("AnimButtonOverlay").setFillColor(colorBlack);
                    AnimButtonView.update();
                } else {
                    // error
                }
            }
        });

        // Implementation of PowerButton to change its properties
        final VectorMasterView PowerButtonView = convertView.findViewById(R.id.PowerButton);
        PowerButtonView.getPathModelByName("PowerButtonOverlay").setFillColor(colorBlack);

        // Set current LED color to the button
        PowerButtonView.getPathModelByName("PowerButton").setFillColor(oneColorToRuleThemAll);
        PowerButtonView.update();

        PowerButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: put powerButton code here
                // TODO: save status of each button indivitually.

                // if button is off -> turn it on
                if(PowerButtonView.getPathModelByName("PowerButtonOverlay").getFillColor()==colorBlack) {
                    PowerButtonView.getPathModelByName("PowerButtonOverlay").setStrokeColor(0xFFFFCA00);
                    PowerButtonView.getPathModelByName("PowerButtonOverlay").setFillColor(colorTransparent);
                    PowerButtonView.getPathModelByName("PowerButton").setStrokeColor(0xFFFFCA00);
                    PowerButtonView.getPathModelByName("PowerButton").setStrokeWidth(3); // change circle width in order to see the yellow color
                    PowerButtonView.update();
                // if button is on -> turn it off
                } else if(PowerButtonView.getPathModelByName("PowerButtonOverlay").getFillColor()==colorTransparent) {
                    PowerButtonView.getPathModelByName("PowerButtonOverlay").setFillColor(colorBlack);
                    PowerButtonView.getPathModelByName("PowerButtonOverlay").setStrokeColor(0xFF000000);
                    PowerButtonView.getPathModelByName("PowerButton").setStrokeColor(0xFF353535);
                    PowerButtonView.update();
                } else {
                    // error
                }

                PowerButtonView.update();
            }
        });

        // implement accessable seekbar to change its properties
        final SeekBar seekBar = accessableView.findViewById(R.id.SeekBar);
        setProgressBarColor(seekBar, oneColorToRuleThemAll);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // Implementation of the TextView to change its properties
        final TextView textView = convertView.findViewById(R.id.Description);
        String item = items.get(position);
        if(item != null) textView.setText(item);

        final int testcolor = Color.parseColor("#FFFFF00F");

        // Declaration of the color picker
        final ColorPickerDialog colorPickerDialog;
        colorPickerDialog = new ColorPickerDialog(ItemListAdapter.this.getContext(), oneColorToRuleThemAll);

        // Implementation of an onClickListener for the textview
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {

                // setup color picker settings
                colorPickerDialog.setAlphaSliderVisible(true);
                colorPickerDialog.setHexValueEnabled(true);
                colorPickerDialog.setTitle("Color Picker");

                // Implementation of an onColorChangeListener
                colorPickerDialog.setOnColorChangedListener(new ColorPickerDialog.OnColorChangedListener() {
                    @Override
                    public void onColorChanged(int color) {
                        // apply the new color to each part of the current listitem
                        setProgressBarColor(seekBar, color);
                        setViewBackgroundColor(accessableView, colorDefault, color);
                        PowerButtonView.getPathModelByName("PowerButton").setFillColor(color);
                        PowerButtonView.update();
                    }
                });

                // bring up the color picker interface
                colorPickerDialog.show();

            }
        });

        return convertView;
    }
}
