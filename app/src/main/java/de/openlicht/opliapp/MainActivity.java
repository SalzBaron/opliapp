package de.openlicht.opliapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // implement Listview and dummy data
        final ListView resultsListView = (ListView) findViewById(R.id.resultListView);
        ArrayList<String> nameAddresses = new ArrayList<>();
        nameAddresses.add("Nachttisch");
        nameAddresses.add("Kochinsel Beleuchtung");
        nameAddresses.add("Leselampe");
        nameAddresses.add("Flurbeleuchtung");
        nameAddresses.add("Wohnzimmer Deckenlampe");
        nameAddresses.add("Büro");
        nameAddresses.add("Nachttisch2");
        nameAddresses.add("Kochinsel Beleuchtung2");
        nameAddresses.add("Leselampe2");
        nameAddresses.add("Flurbeleuchtung2");
        nameAddresses.add("Wohnzimmer Deckenlampe2");
        nameAddresses.add("Büro2");

        // Adapter to transmit one content at a time
        ItemListAdapter adapter = new ItemListAdapter(this, R.layout.list_item, nameAddresses);

        // Save current state
        //Parcelable state = resultsListView.onSaveInstanceState();

        // set the adapter and put it to work
        resultsListView.setAdapter(adapter);

        // Reload saved state
        //resultsListView.onRestoreInstanceState(state);

        // implement a SearchBtn
        final Button searchBtn = (Button) findViewById(R.id.SearchBtn);

        // onClick() function of SearchBtn
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "SearchBtn.onClick(): clicked");
                Toast.makeText(getApplicationContext(), "Searching for Devices", Toast.LENGTH_SHORT).show();
            }
        });

        // onTouch() funtions of SearchBtn TODO: This was just for testing purposes, remove if not needed
        searchBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    // Button Pressed
                    Log.d(TAG, "SearchBtn.onTouch(): Button Pressed");
                    Toast.makeText(getApplicationContext(), "button pressed", Toast.LENGTH_SHORT).show();
                }
                if(motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    // Button Released
                    Log.d(TAG, "SearchBtn.onTouch(): Button Released");
                    Toast.makeText(getApplicationContext(), "button released", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
    }
}
